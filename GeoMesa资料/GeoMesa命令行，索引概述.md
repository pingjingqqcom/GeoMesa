﻿@[toc](GeoMesa)
# 一、GeoMesa命令行

## 查看classpath
执行“classpath”命令，将会返回当前命令行工具的所有classpath信息。

```
bin/geomesa-hbase classpath
```
## 创建表
执行“create-schema”命令创建表，创建表时至少要指定目录名称与表名称，以及表规格。

```
bin/geomesa-hbase create-schema -c geomesa -f test -s Who:String,What:java.lang.Long,When:Date,*Where:Point:srid=4326,Why:String
```

## 描述表
执行“describe-schema”命令获取表描述信息，描述表信息时必须要指定目录名称与表名称。

```
bin/geomesa-hbase describe-schema -c geomesa -f test
```

## 批量导入数据
执行“ingest”命令批量导入数据，导入时需要指定目录名称，表名称，表规格，以及相应的数据转换器等。

数据(车牌号，车辆颜色，经度，维度，时间)：data.csv，并将数据表放在data文件夹中。

```
AAA,red,113.918417,22.505892,2017-04-09 18:03:46
BBB,white,113.960719,22.556511,2017-04-24 07:38:47
CCC,blue,114.088333,22.637222,2017-04-23 15:07:54
DDD,yellow,114.195456,22.596103,2017-04-21 21:27:06
EEE,black,113.897614,22.551331,2017-04-09 09:34:48
```

表结构定义：myschema.sft，并将myschema.sft放在geomesa命令行工具的conf文件夹中。

```
geomesa.sfts.cars = {
   attributes = [
        { name = "carid", type = "String", index = true }
        { name = "color", type = "String", index = false }
        { name = "time", type = "Date",   index = false }
        { name = "geom", type = "Point",  index = true,srid = 4326,default = true }
   ]
}
```
执行命令导入数据：

```
bin/geomesa-hbase ingest -c geomesa -C conf/myconvertor.convert -s conf/myschema.sft data/data.csv
```

## 解释查询
执行“explain”命令获取指定查询语句执行计划的解释说明，解释语句时必须指定目录名称和表名称，以及给定查询语句。

```
bin/geomesa-hbase explain -c geomesa -f cars -q "carid = 'BBB'"
```

## 统计分析
执行“stats-analyze”命令对数据表进行统计分析，同时还可以进一步执行“stats-bounds”，“stats-count”，“stats-histogram”，“stats-top-k”命令对数据表做更详细的统计。

```
bin/geomesa-hbase stats-analyze -c geomesa -f cars
bin/geomesa-hbase stats-bounds -c geomesa -f cars
bin/geomesa-hbase stats-count -c geomesa -f cars
bin/geomesa-hbase stats-histogram -c geomesa -f cars
bin/geomesa-hbase stats-top-k -c geomesa -f cars
```

## 导出feature
执行“export”命令导出feature，导出时必须指定目录名称和表名称，同时还可以根据指定的查询语句进行导出。

```
bin/geomesa-hbase export -c geomesa -f cars -q "carid = 'BBB'"
```

## 删除feature
执行“delete-features”命令删除feature，删除时必须指定目录名称和表名称，同时还可以根据指定的查询语句进行删除。

```
bin/geomesa-hbase delete-features -c geomesa -f cars -q "carid = 'BBB'"
```

## 获取目录中的全部表的名称
执行“get-type-names”命令获取指定目录中的表名称。

```
bin/geomesa-hbase get-type-names -c geomesa
```


## 删除表
执行“remove-schema”命令删除表，删除表示至少要指定表所在的目录与表名称。

```
bin/geomesa-hbase remove-schema -c geomesa -f test
bin/geomesa-hbase remove-schema -c geomesa -f cars
```

## 删除目录
执行“delete-catalog”命令删除指定的目录。

```
bin/geomesa-hbase delete-catalog -c geomesa
```

# 二、GeoMesa 索引
GeoMesa将为给定的SimpleFeatureType模式创建各种索引。这允许我们以优化的方式回答各种查询。GeoMesa将尽最大努力确定用于索引的属性。要使用的属性也可以指定为SimpleFeatureType。

## 索引概述

**Z2 [ z2]** - Z2索引使用二维Z阶曲线来索引点数据的纬度和经度。如果要素类型具有几何类型，则将创建此索引 Point。这用于有效地回答具有空间组件但没有时间组件的查询。 

**Z3 [ z3]** - Z3索引使用三维Z阶曲线来索引点数据的纬度，经度和时间。如果要素类型具有几何类型Point且具有时间属性，则将创建此索引。这用于有效地回答具有空间和时间组件的查询。 

**XZ2 [ xz2]** - XZ2索引使用XZ-ordering [1]的二维实现来索引非点数据的纬度和经度。XZ排序是Z-排序的扩展，设计用于空间扩展对象（即非点几何，如线串或多边形）。如果要素类型具有非Point几何图形，则将创建此索引。这用于有效地回答具有空间组件但没有时间组件的查询。 

**XZ3 [ xz3]** - XZ3索引使用XZ-ordering [1]的三维实现来索引非点数据的纬度，经度和时间。如果要素类型具有非Point几何并且具有时间属性，则将创建此索引。这用于有效地回答具有空间和时间组件的查询。 

**Record / ID [ id]** - 记录索引使用功能ID作为主键。它用于ID的任何查询。此外，某些属性查询可能最终从记录索引中检索数据。 

**Attribute [ attr]** - 属性索引使用属性值作为主索引键。这允许在没有时空组件的情况下快速检索查询。属性索引包括辅助时空密钥，其可以改进具有多个谓词的查询。

## 空间指数（Z2 / XZ2）
如果SimpleFeatureType有Geometry型属性（Point，LineString，Polygon等），GeoMesa将创建在该属性的空间索引。如果有多个Geometry-type属性，则使用默认属性。默认几何体通常*在SimpleFeatureType字符串中使用前缀指定，并且是返回的前缀 SimpleFeatureType.getGeometryDescriptor。

## 时空指数（Z3 / XZ3）
如果SimpleFeatureType同时具有Geometry-type属性和Date属性，GeoMesa将在这些属性上创建时空索引。使用的Geometry-type属性与上面的空间索引相同。Date选择的属性将是声明的第一个属性，或者可以显式设置。

## ID索引
GeoMesa将始终创建ID索引SimpleFeature.getID()。

## 属性索引
有些查询使用默认索引很难回答。例如，对于Twitter数据，您可能希望返回给定用户的所有推文。要加速此类查询，可以单独索引简单要素类型中的任何属性。
