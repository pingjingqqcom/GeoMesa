/*
package com.geomesa.spark.SparkCore

import com.geomesa.spark.SparkJTS.GeometryFactory
import com.vividsolutions.jts.geom.Geometry
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.{StringType, StructField, StructType}
import org.locationtech.jts.geom.GeometryFactory

/**
  * 构建UDF的方式
  */
object ByUdfFunction {

  private val factory = GeometryFactory.getInstance

  def main(args: Array[String]): Unit = {

    import org.locationtech.geomesa.spark.jts._
    //spark
    val spark: SparkSession = {
      SparkSession.builder()
        .appName("ByUdfFunction")
        .master("local[*]")
        .getOrCreate()
        //需注入spark.jts._包
        .withJTS
    }

    //UDF
    val schema = StructType(Array(
      StructField("id", StringType),
      //显示路线名称
      /* StructField("properties",StructType(Array(
         StructField("NAME1",StringType)
       ))),*/
      StructField("geometry", StructType(Array(
        StructField("type", StringType),
        StructField("coordinates", StringType)
      )))
    ))

    val dataFile = this.getClass.getClassLoader.getResource("line.txt").getPath
    import spark.implicits._
    val df = spark.read
      .schema(schema)
      .json(dataFile)
    //.show(5)
    //.printSchema()

    val alertDf = df.withColumn("type", $"geometry.type")
      .withColumn("coordinates", $"geometry.coordinates")
    /* .withColumn("geometryType",st_distance($"",$"geometry.type"))
     .show(5)*/

    //获取点坐标
    val row = alertDf.first()
    val gf = new GeometryFactory()
    val strCoordinate = row.get(3).toString
    val str = strCoordinate.substring(1, strCoordinate.length() - 1)
    val lineString = strCoordinate
      .replace("[", "")
      .replace("]", "")
      .split(",")
    /*121.97738334,37.47710858,121.97738305,37.47710848,121.97728053,37.47707084,121.97634265,37.47672591*/
    for (line <- lineString) {
      if (line.startsWith("1") || line.startsWith("3")) {
        println(line)
      }
    }
    /*[[121.97738334,37.47710858],[121.97738305,37.47710848],[121.97728053,37.47707084]]*/
    val coordinates: Geometry = factory.buildGeo("LINESTRING " + "(121.97738334 37.47710858,121.97738305 37.47710848,121.97728053 37.47707084)")
    println(coordinates)



    /*    root
        |-- geometry: struct (nullable = true)
        |    |-- coordinates: array (nullable = true)
        |    |    |-- element: array (containsNull = true)
        |    |    |    |-- element: array (containsNull = true)
        |    |    |    |    |-- element: double (containsNull = true)
        |    |-- type: string (nullable = true)
        |-- geometry_name: string (nullable = true)*/


    /*+--------+--------------------+---------------+--------------------+
    |      id|            geometry|           type|         coordinates|
      +--------+--------------------+---------------+--------------------+
    |sdgsmc.1|[MultiLineString,...|MultiLineString|[[[1.357846020181...|
    |sdgsmc.2|[MultiLineString,...|MultiLineString|[[[1.319414533241...|
    |sdgsmc.3|[MultiLineString,...|MultiLineString|[[[1.287772131522...|
    |sdgsmc.4|[MultiLineString,...|MultiLineString|[[[1.319625761819...|
    |sdgsmc.5|[MultiLineString,...|MultiLineString|[[[1.322615028157...|
    +--------+--------------------+---------------+--------------------+*/

  }
}
*/
