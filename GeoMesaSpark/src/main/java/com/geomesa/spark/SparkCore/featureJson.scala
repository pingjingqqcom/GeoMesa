package com.geomesa.spark.SparkCore

import java.io.{BufferedReader, File, FileInputStream, FileReader, InputStreamReader}

import org.geotools.data.simple.SimpleFeatureIterator
import org.geotools.geojson.feature.FeatureJSON
import org.geotools.geojson.geom.GeometryJSON
import org.opengis.feature.simple.SimpleFeatureType

object featureJson {
  def main(args: Array[String]): Unit = {
    /*    val featureJSON = new FeatureJSON(new GeometryJSON(15))
        val inputStreamReader = new InputStreamReader(this.getClass.getResourceAsStream("/gsmc.txt"))
        val bufferedReader = new BufferedReader(inputStreamReader)
        val featureCollection = featureJSON.readFeatureCollection(bufferedReader)*/

    import org.geotools.geojson.feature.FeatureJSON
    val reader = new FileReader("D:/GitProjects/GeoMesa/GeoMesaSpark/src/main/resources/gsmc.txt")
    val bufferReader = new BufferedReader(reader)
    val dict = bufferReader.readLine()
    bufferReader.close()
    reader.close()

    val fjson = new FeatureJSON(new GeometryJSON(15))
    val features = fjson.readFeatureCollection(dict)


    println("AAA:"+features.features())
    println("BBB:"+features.getSchema())

  }
}
