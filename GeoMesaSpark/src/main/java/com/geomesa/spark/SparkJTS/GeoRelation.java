package com.geomesa.spark.SparkJTS;

import com.vividsolutions.jts.algorithm.CGAlgorithms;
import com.vividsolutions.jts.algorithm.PointLocator;
import com.vividsolutions.jts.geom.Coordinate;

public class GeoRelation {

    private static GeometryFactory factory = GeometryFactory.getInstance();


    public static void main(String[] args) {
        //计算线段AB到CD的最短距离
        double cgAlgorithms = CGAlgorithms.distanceLineLine(
                new Coordinate(1, 1),
                new Coordinate(2, 2),
                new Coordinate(2, 0),
                new Coordinate(3, 0));

        System.out.println("计算线段AB到CD的最短距离：" + cgAlgorithms);

        //计算点到CD的最短距离
        double cgAlgorithms1 = CGAlgorithms.distancePointLine(new Coordinate(0, 0), new Coordinate[]{new Coordinate(1, 1), new Coordinate(2, 0), new Coordinate(2, 2), new Coordinate(1, 1)});
        System.out.println("计算点到CD的最短距离：" + cgAlgorithms1);

        //点是否在线段上
        boolean isLineFlag = CGAlgorithms.isOnLine(new Coordinate(2, 3), new Coordinate[]{new Coordinate(2, 0), new Coordinate(2, 2), new Coordinate(1, 1)});
        System.out.println("点是否在线段上：" + isLineFlag);

        //判断点(point) 和 对象(Geometry)之间的关系
        //public final static int INTERIOR = 0; 在内部
        //public final static int BOUNDARY = 1; 在边界上
        //public final static int EXTERIOR = 2; 在外部
        //public final static int NONE = -1;
        PointLocator pl = new PointLocator();
        //在线上
        int onLine = pl.locate(new Coordinate(0, 0), factory.buildGeo("LINESTRING (0 0,1 1,2 2, 5 5,0 5)"));
        System.out.println("在线上：" + onLine);
        //在面上
        //System.out.println(pl.locate(new Coordinate(0, 0), factory.buildGeo("POLYGON ((0 0,1 1,2 2,5 5,0 5,0 0))")));
        //在面内
        //System.out.println(pl.locate(new Coordinate(1, 3), factory.buildGeo("POLYGON ((0 0,1 1,2 2,5 5,0 5,0 0))")));
        //在面外
        //System.out.println(pl.locate(new Coordinate(10, 10), factory.buildGeo("POLYGON ((0 0,1 1,2 2,5 5,0 5,0 0))")));
    }
}
