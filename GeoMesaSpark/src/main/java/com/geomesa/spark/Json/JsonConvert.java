package com.geomesa.spark.Json;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.util.ArrayList;

public class JsonConvert {

    /**
     * @param args
     */
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<String>();
        // 读取原始json文件并进行操作和输出
        try {
            BufferedReader br = new BufferedReader(new FileReader(
                    "src/json/gsmc.txt"));// 读取原始json文件
            BufferedWriter bw = new BufferedWriter(new FileWriter(
                    "src/json/HK_new.json"));// 输出新的json文件
            String s = null, ws = null;
            while ((s = br.readLine()) != null) {
                // System.out.println(s);
                try {
                    JSONObject dataJson = new JSONObject(s);// 创建一个包含原始json串的json对象
                    JSONArray features = dataJson.getJSONArray("features");// 找到features json数组
                    for (int i = 0; i < features.length(); i++) {
                        JSONObject info = features.getJSONObject(i);// 获取features数组的第i个json对象
                        JSONObject properties = info.getJSONObject("properties");// 找到properties的json对象
                        JSONObject geometry = info.getJSONObject("geometry");
                        String type = geometry.getString("type");
                        System.out.println(type);
                        //String name = properties.getString("name");// 读取properties对象里的name字段值
                        //System.out.println(name);
                        //properties.put("NAMEID", list.get(i));// 添加NAMEID字段
                        //System.out.println(properties.getString("NAMEID"));
                        properties.remove("ISO");// 删除ISO字段
                        properties.remove("timeStamp");// 删除ISO字`段
                    }
                    ws = dataJson.toString();
                    System.out.println(ws);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            bw.write(ws);
            bw.flush();
            br.close();
            bw.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}

